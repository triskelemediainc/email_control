<?php
/**
 * @file
 *
 * Provides various controls for outgoing emails.
 *
 */

/**
 * Implements hook_menu().
 *
 * Provide admin interface for the various Dx areas of concern.
 */
function email_control_menu() {
  $items = [];

  $items['admin/config/system/email_control'] = [
    'title' => 'Email Control',
    'description' => 'Configure the Email Control module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['email_control_config_form'],
    'access arguments' => ['access administration pages'],
    'type' => MENU_NORMAL_ITEM,
  ];

  return $items;
}

/**
 * Admin form for Email Control
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function email_control_config_form($form, &$form_state) {
  global $user;
  $form = [];

  $mail_modules = [];
  foreach (module_list(FALSE, FALSE, TRUE) as $module) {
    $info = system_get_info('module', $module);
    $mail_modules[$module] = $info['name'] . ' (' . $module . ')';
  }

  $form['email_control_send_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email Sending'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['email_control_send_group']['email_control_send'] = [
    '#type' => 'radios',
    '#title' => t('Send Emails?'),
    '#default_value' => variable_get('email_control_send', 'none'),
    '#options' => [
      'none' => 'Send No Emails',
      'address' => 'Send To Specific Email Address',
      'user' => 'Send To Logged In User\'s Address',
      'all' => 'Send All; Don\'t Interfere With Any Emails',
    ],
    '#description' => t('Select how the No Email Sending module should behave with regard to outgoing emails.'),
  ];

  $form['email_control_send_group']['email_control_send_email_name'] = [
    '#type' => 'textfield',
    '#title' => t('Email Name'),
    '#description' => t('The value in this field isn\'t validated; you are a trusted admin user, so please get this right.'),
    '#default_value' => variable_get('email_control_send_email_name', ''),
    '#maxlength' => 128,
    '#states' => [
      'visible' => [
        ':input[name="email_control_send"]' => ['value' => 'address'],
      ],
    ],
  ];

  $form['email_control_send_group']['email_control_send_email_address'] = [
    '#type' => 'textfield',
    '#title' => t('Email Address'),
    '#description' => t('The value in this field isn\'t validated; you are a trusted admin user, so please get this right.'),
    '#default_value' => variable_get('email_control_send_email_address', ''),
    '#maxlength' => 128,
    '#states' => [
      'visible' => [
        ':input[name="email_control_send"]' => ['value' => 'address'],
      ],
    ],
  ];

  $form['email_control_send_group']['email_control_send_user_name'] = [
    '#type' => 'textfield',
    '#title' => t('Logged In User\'s Name'),
    '#description' => t('This will change to depending on the logged in user at the time email is sent.'),
    '#default_value' => $user->name,
    '#maxlength' => 128,
    '#disabled' => TRUE,
    '#states' => [
      'visible' => [
        ':input[name="email_control_send"]' => ['value' => 'user'],
      ],
    ],
  ];

  $form['email_control_send_group']['email_control_send_user_address'] = [
    '#type' => 'textfield',
    '#title' => t('Logged In User\'s Email Address'),
    '#description' => t('This will change to depending on the logged in user at the time email is sent.'),
    '#default_value' => $user->mail,
    '#maxlength' => 128,
    '#disabled' => TRUE,
    '#states' => [
      'visible' => [
        ':input[name="email_control_send"]' => ['value' => 'user'],
      ],
    ],
  ];

  $form['email_control_send_group']['email_control_send_suppress'] = [
    '#type' => 'radios',
    '#title' => t('Suppress Multiple Emails?'),
    '#default_value' => variable_get('email_control_send_suppress', 'none'),
    '#options' => [
      'none' => 'Don\'t Block',
      'one' => 'Don\'t Send More Than One',
      'duplicate_subject' => 'Don\'t Send If Same Subject',
      'duplicate_subject_body' => 'Don\'t Send If Same Subject And Body',
    ],
    '#description' => t('Select how the No Email Sending module should deal with multiple emails in a single page request.'),
    '#states' => [
      'invisible' => [
        ':input[name="email_control_send"]' => [
          ['value' => 'none'],
          ['value' => 'all'],
        ],
      ],
    ],
  ];

  $form['email_control_send_group']['email_control_send_prepend_names'] = [
    '#type' => 'radios',
    '#title' => t('Add Overwritten Names To Email Body?'),
    '#default_value' => variable_get('email_control_send_prepend_names', 'none'),
    '#options' => [
      'none' => 'Don\'t Add Names To Email Body',
      'include' => 'Include Overwritten Names In Email Body',
    ],
    '#description' => t('Select if the No Email Sending module should add text to the top of email indicating what name the email was originaly addressed to.'),
    '#states' => [
      'invisible' => [
        ':input[name="email_control_send"]' => [
          ['value' => 'none'],
          ['value' => 'all'],
        ],
      ],
    ],
  ];

  $form['email_control_send_group']['email_control_send_modules'] = [
    '#type' => 'select',
    '#title' => t('Modules To Effect Sending Emails From'),
    '#description' => t('Select the modules that sending emails will be altered for. Selecting no entries means that all outgoing emails are effected.'),
    '#default_value' => variable_get('email_control_send_modules', []),
    '#options' => $mail_modules,
    '#multiple' => TRUE,
    '#size' => 20,
    '#states' => [
      'visible' => [
        ':input[name="email_control_send"]' => [
          ['value' => 'address'],
          ['value' => 'user'],
        ]
      ],
    ],
  ];

  $form['email_control_header_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email Headers'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['email_control_header_group']['email_control_header'] = [
    '#type' => 'radios',
    '#title' => t('Format Emails?'),
    '#default_value' => variable_get('email_control_header', 'none'),
    '#options' => [
      'none' => 'Add No Headers To Emails',
      'add' => 'Add Headers To Emails',
    ],
    '#description' => t('Select whether the No Email Sending module should alter the headers of emails.'),
  ];

  $form['email_control_header_group']['email_control_header_html'] = [
    '#type' => 'radios',
    '#title' => t('HTML Headers'),
    '#description' => t('Select how the No Email Sending module should alter the HTML format headers (i.e. "Content-Type" and "Content-Transfer-Encoding") of emails.'),
    '#default_value' => variable_get('email_control_header_html', []),
    '#options' => [
      'none' => 'Don\'t Add HTML Headers.',
      'html' => 'Add HTML Headers.',
    ],
    '#states' => [
      'visible' => [
        ':input[name="email_control_header"]' => ['value' => 'add'],
      ],
    ],
  ];

  $form['email_control_header_group']['email_control_header_x_mailer'] = [
    '#type' => 'radios',
    '#title' => t('X-Mailer Headers'),
    '#default_value' => variable_get('email_control_header_x_mailer', 'none'),
    '#options' => [
      'none' => 'Don\'t Add X-Mailer Header.',
      'module' => 'Add X-Mailer Header based on the module name.',
      'custom' => 'Add custom X-Mailer Header.',
    ],
    '#description' => t('Select how the No Email Sending module should alter the X-Mailer headers of emails.'),
    '#states' => [
      'visible' => [
        ':input[name="email_control_header"]' => ['value' => 'add'],
      ],
    ],
  ];

  $form['email_control_header_group']['email_control_header_x_mailer_custom'] = [
    '#type' => 'textfield',
    '#title' => t('Custome X-Mailer Header'),
    '#description' => t('Indicated the X-Mailer header to include on outgoing emails'),
    '#default_value' => variable_get('email_control_header_x_mailer_custom', ''),
    '#maxlength' => 128,
    '#states' => [
      'visible' => [
        ':input[name="email_control_header"]' => ['value' => 'add'],
        ':input[name="email_control_header_x_mailer"]' => ['value' => 'custom'],
      ],
    ],
  ];

  $form['email_control_header_group']['email_control_header_modules'] = [
    '#type' => 'select',
    '#title' => t('Modules To Effect Headers For'),
    '#description' => t('Select the modules that email formatting headers will be altered for. Selecting no entries means that all outgoing emails are altered.'),
    '#default_value' => variable_get('email_control_header_modules', []),
    '#options' => $mail_modules,
    '#multiple' => TRUE,
    '#size' => 20,
    '#states' => [
      'visible' => [
        ':input[name="email_control_header"]' => ['value' => 'add'],
      ],
    ],
  ];

  return system_settings_form($form);
}

/**
 * Validation of Email Control's Admin form
 *
 * @param $form
 * @param $form_state
 */
function email_control_config_form_validate($form, &$form_state) {
  global $user;
  $pass = TRUE;

  switch ($form_state['values']['email_control_send']) {
    case 'none':
      drupal_set_message(t('No email will be sent out.'), 'warning');
      break;
    case 'address':
      if (!valid_email_address($form_state['values']['email_control_send_email_address'])) {
        form_set_error(
          'email_control_send_email_address',
          t('A valid email address is required.')
        );
        $pass = FALSE;
      }
      if ($form_state['values']['email_control_send_email_name'] !== htmlentities($form_state['values']['email_control_send_email_name'])) {
        form_set_error(
          'email_control_send_email_name',
          t('The Email Name field contains invalid characters.')
        );
        $pass = FALSE;
      }
      if ($pass) {
        drupal_set_message(
          t(
            'All emails will be re-routed to the "%email" email address.',
            [
              '%email' => email_control_make_email_address(
                $form_state['values']['email_control_send_email_address'],
                $form_state['values']['email_control_send_email_name']
              ),
            ]
          ),
          'warning');
      }
      break;
    case 'user':
      drupal_set_message(
        t(
          'All emails will be re-routed to the logged in user\'s email address; e.g. %email.',
          [
            '%email' => email_control_make_email_address(
              $user->mail,
              $user->name
            ),
          ]
        ),
        'warning');
      break;
    case 'all':
      drupal_set_message(t('All emails will be allowed out without interference from the Email Control module.'), 'warning');
      break;
  }

  if (
    'address' == $form_state['values']['email_control_send']
    || 'user' == $form_state['values']['email_control_send']
  ) {
    switch ($form_state['values']['email_control_send_suppress']) {
      case 'none':
        break;
      case 'one':
        drupal_set_message(t('Only first email per page request will be sent.'), 'warning');
        break;
      case 'duplicate_subject':
        drupal_set_message(t('Only emails with different subjects will be sent.'), 'warning');
        break;
      case 'duplicate_subject_body':
        drupal_set_message(t('Only emails with different subjects and body content will be sent.'), 'warning');
        break;
    }
  }
}

/**
 * Implements hook_mail_alter()
 *
 * Checks whether the Email Control module should block or modify outgoing
 * emails, and does so.
 *
 * @param $message
 */
function email_control_mail_alter(&$message) {
  global $user;
  static $previous_messages = FALSE;
  static $email_control_to = FALSE;
  static $email_could_not_send_message = FALSE;

  if (FALSE === $previous_messages) {
    $previous_messages = [];
  }

  $email_control_send = variable_get('email_control_send', 'none');

  // The code that last called 'drupal_mail' relative the present execution
  // stack is in the module we need to target, so fetch that module name
  // here for testing.
  $module = email_control_module_from_function_in_stack('drupal_mail');

  if (
    'all' != $email_control_send
    && (
      !($modules = variable_get('email_control_send_modules', []))
      || isset($modules[$module])
    )
  ) {
    if ('none' != $email_control_send) {
      switch (variable_get('email_control_send_suppress', 'none')) {
        case 'none':
          break;
        case 'one':
          if (!empty($previous_messages)) {
            $email_control_send = 'none';
          }
          break;
        case 'duplicate_subject':
          foreach ($previous_messages as $previous_message) {
            if ($previous_message['subject'] == $message['subject']) {
              $email_control_send = 'none';
              break;
            }
          }
          break;
        case 'duplicate_subject_body':
          foreach ($previous_messages as $previous_message) {
            if (
              $previous_message['subject'] == $message['subject']
              && $previous_message['body'] == $message['body']
            ) {
              $email_control_send = 'none';
              break;
            }
          }
          break;
      }

      switch (variable_get('email_control_send_prepend_names', 'none')) {
        case 'none':
          break;
        case 'include':
          array_unshift(
            $message['body'],
            '<div>'
              . t(
                'Email Control redirected this email orginally addressed to %email',
                [
                  '%email' => $message['to'],
                ]
              )
              . ' </div><hr />'
          );
          break;
      }

      switch ($email_control_send) {
        case 'none':
          break;
        case 'address':
          if (FALSE === $email_control_to) {
            $to_name = variable_get('email_control_send_email_name', FALSE);
            $to_address = variable_get('email_control_send_email_address', FALSE);
          }
          break;
        case 'user':
          if (
            !$email_could_not_send_message
            && !valid_email_address($user->mail)
          ) {
            drupal_set_message(
              t(
                'The Email Control module not send to invalid email address: %email',
                [
                  '%email' => $user->mail,
                ]
              ),
              'error'
            );
            $email_could_not_send_message = TRUE;
            $email_control_send = 'none';
          }

          if ($email_could_not_send_message) {
            $email_control_send = 'none';
          }
          elseif (FALSE === $email_control_to) {
            $to_name = $user->name;
            $to_address = $user->mail;
          }
          break;
      }

      if (isset($to_name)) {
        if (
          !empty($to_name)
          && !empty($to_address)
        ) {
          $email_control_to = email_control_make_email_address($to_address, $to_name);
        }
        else {
          $email_control_send = 'none';
        }
      }
    }

    if (
      'none' == $email_control_send
      || FALSE === $email_control_to
    ) {
      $message['send'] = FALSE;
    }
    else {
      $message['to'] = $email_control_to;
    }

    $previous_messages[] = $message;
  }

  // Add the HTML headers to emails.
  if (
    'none' != variable_get('email_control_header', 'none')
    && (
      !($modules = variable_get('email_control_header_modules', []))
      || isset($modules[$module])
    )
  ) {

    switch (variable_get('email_control_header_html', 'none')) {
      case 'none':
        break;
      case 'html':
        $message['headers']['Content-Type'] = 'text/html; charset=ISO-8859-1';
        $message['headers']['Content-Transfer-Encoding'] = 'base64';
        break;

    }

    switch (variable_get('email_control_header_x_mailer', 'none')) {
      case 'none':
        break;
      case 'module':
        if (
          !!($info = system_get_info('module', $module))
          && !!$info['name']
        ) {
          $message['headers']['X-Mailer'] = $info['name'];
        }
        break;
      case 'custom':
        if (!!($custom = variable_get('email_control_header_x_mailer_custom', FALSE))) {
          $message['headers']['X-Mailer'] = $custom;
        }
        break;
    }
  }
}

/**
 * Format name and email into email address.
 *
 * If name is blank, then just the email address is returned, otherwise the
 * returned string is the quoted name and the bracketed email. It as assumed
 * that the email and name have already been validated and sanitized.
 *
 * @param $address
 * @param $name
 * @return string
 */
function email_control_make_email_address($address, $name) {
  return
    empty($name)
      ? $address
      : '"' . $name . '" <' . $address . '>';

}

/**
 * Find module name that executed a function most recently in the call stack.
 *
 * @param $function
 * @return bool|mixed
 */
function email_control_module_from_function_in_stack($function) {
  foreach (debug_backtrace() as $dbt) {
    if ($function !== $dbt['function']) {
      continue;
    }

    return email_control_module_from_path($dbt['file']);
    break;
  }

  return FALSE;
}

/**
 * Fetch the module name associated with a given path.
 *
 * @param $path
 * @return bool|mixed
 */
function email_control_module_from_path($path) {
  static $paths = FALSE;

  // build collection of paths and their module names.
  if (!$paths) {
    $paths = [];
    foreach (module_list(FALSE, FALSE, TRUE) as $module) {
      $paths[drupal_get_path('module', $module)] = $module;
    }
  }

  // slice off the non-drupal part of the path.
  $path = preg_replace('#^' . filesystem_base_path() . DIRECTORY_SEPARATOR . '#', '', $path);

  // iteratively chop down the path until either a match is found for a module
  // or the path can't be reduced further
  $previous_path = $path;
  while (!!($path = preg_replace('#' . DIRECTORY_SEPARATOR . '[^' . DIRECTORY_SEPARATOR . ']*$#', '', $path))) {
    if ($previous_path == $path) {
      break;
    }

    if (isset($paths[$path])) {
      return $paths[$path];
    }
    $previous_path = $path;
  }

  return FALSE;
}

/**
 * Returns the base filesystem path of the Drupal installation.
 *
 * @author: Radon8472
 * @version: 1.0 (2013-10-19)
 *
 * @return string: absolute local filesystem path of the Drupal installation.
 */
function filesystem_base_path() {
  if(!isset($GLOBALS['filesystem_base_path'])) {
    $search = "includes" . DIRECTORY_SEPARATOR . "bootstrap.inc";

    // Walk directories up until we find the $search-path (which is relative to root)
    for($path=dirname(__FILE__); !file_exists($path.DIRECTORY_SEPARATOR.$search); $path.= DIRECTORY_SEPARATOR."..") {
      // no need to do anything
    }
    // store the path if it one was found
    $GLOBALS['filesystem_base_path'] = realpath($path);
  }
  return $GLOBALS['filesystem_base_path'];
}